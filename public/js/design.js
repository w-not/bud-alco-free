$(document).ready(function() {

    $("#phone").mask("+7 (999) 999-99-99");
    $("#birthday").mask("99.99.9999");


	// Check 18 years
	if($('#modal_age').length) {

		// Check cookies
		if ($.cookie('check_18year') == 'yes') {
			$('.modal_age').modal('hide');
		} else {
			$('.modal_age').modal('show');

			$('.modal_age').on('shown.bs.modal', function() {

				$('.modal_age .inp-line:first-child .inp-style').focus();

				$('.modal_age .inp-line .inp-style').keyup(function () {
				    if(this.value.length == this.maxLength) {
				      $(this).parent().next().find('.inp-style').focus();
				    }
				});

			});
		}

		// Validate 18 years
		$.validator.addMethod('check_date_of_birth', function(value, element) {

		    var day = $('.inp-day').val();
		    var month = $('.inp-month').val();
		    var year = $('.inp-year').val();
		    var age =  18;

		    var mydate = new Date();
		    mydate.setFullYear(year, month-1, day-1);

		    var currdate = new Date();
		    currdate.setFullYear(currdate.getFullYear() - age);

		    return currdate > mydate;

		}, 'You must be at least 18 years of age.');


		// Validate 18 years form
		$('#check_18year').validate({
			//errorClass: 'inp-error',
			errorElement: 'label',

			highlight: function (element, errorClass) { 
	            $(element).parent().addClass('inp-error'); 
	        }, 
	        
	        unhighlight: function (element, errorClass) { 
	            $(element).parent().removeClass('inp-error'); 
	        },

			rules: {
				age_day: {
					required: true,
					minlength: 1,
					maxlength: 2,
					digits: true,
					range: [1, 31],
				},

				age_month: {
					required: true,
					minlength: 1,
					maxlength: 2,
					digits: true,
					range: [1, 12],
				},

				age_year: {
					required: true,
					minlength: 4,
					maxlength: 4,
					digits: true,
					check_date_of_birth: true,
				},
			},
			
			messages: {
				age_day: 'Введите правильно день',

				age_month: 'Введите правильно месяц',

				age_year: {
					required: 'Введите правильно год',
					minlength: 'Введите правильно год',
					check_date_of_birth: 'Извините, сайт только для совершеннолетних',
				},
			},


			submitHandler: function(form) {
				$.cookie('check_18year', 'yes', {expires: 30, path: '/'});

				$('.modal_age').modal('hide');
			}

		});
	}

});