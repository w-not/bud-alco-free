<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class SendTestGetPrizeMail
 */
class SendTestGetPrizeMail extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'send:testgetprize';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$email = $this->argument('email');

		Mail::send('emails.get_prize', ['hash' => 'fakehash'], function($msg) use ($email){
			$msg->to($email)
				->subject('=?utf-8?B?' . base64_encode('Выдача призов') . '?=');
		});
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('email', InputArgument::REQUIRED, 'Your email.'),
		);
	}

}
