<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class SendGetPrizeMail
 */
class SendGetPrizeMail extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'send:getprize';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send get_prize mail template.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$query = (new Ticket)->newQuery()->where('code_sent', 1)->where('id', '>', 140);

		$total = $query->count();
		$count = 0;
		$progressBar = $this->getHelperSet()->get('progress');
		$progressBar->start($this->getOutput(), $total);

		$query->chunk(100, function($chunk) use ($progressBar, &$count){
			foreach ($chunk as $ticket){
				if ($this->send($ticket)){
					++$count;
				}
				$progressBar->advance();
				sleep(1);
			}
		});

		$this->info('Successfully sent ' . $count . ' from ' . $total);
	}

	/**
	 * @param Ticket $ticket
	 * @return bool
	 */
	protected function send(Ticket $ticket)
	{
		if (!$ticket->email){
			$this->error("Ticket `{$ticket->id}` without email");
			return false;
		}
		try {
			return Mail::send('emails.get_prize', $ticket->toArray(), function ($msg) use ($ticket) {
				$msg->to($ticket->email, $ticket->first_name)
					->subject('=?utf-8?B?' . base64_encode('Выдача призов') . '?=');
			});
		} catch (\Exception $e){
			return false;
		}
	}
}
