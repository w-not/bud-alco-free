<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>BUD</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style type="text/css">
        @font-face {
            font-family: 'bud_bold_cyrregular';
            src: url('fonts/bud_bold_cyr.eot');
            src: url('fonts/bud_bold_cyr.eot?#iefix') format('embedded-opentype'),
            url('fonts/bud_bold_cyr.woff2') format('woff2'),
            url('fonts/bud_bold_cyr.woff') format('woff'),
            url('fonts/bud_bold_cyr.ttf') format('truetype'),
            url('fonts/bud_bold_cyr.svg#bud_bold_cyrregular') format('svg');
            font-weight: normal;
            font-style: normal;

        }

        @font-face {
            font-family: 'pf_din_text_cond_prolight';
            src: url('fonts/pfdintextcondpro-light.eot');
            src: url('fonts/pfdintextcondpro-light.eot?#iefix') format('embedded-opentype'),
            url('fonts/pfdintextcondpro-light.woff2') format('woff2'),
            url('fonts/pfdintextcondpro-light.woff') format('woff'),
            url('fonts/pfdintextcondpro-light.ttf') format('truetype'),
            url('fonts/pfdintextcondpro-light.svg#pf_din_text_cond_prolight') format('svg');
            font-weight: normal;
            font-style: normal;
        }
    </style>
</head>
<body bgcolor="#ffffff">

<table width="638" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">

    <tr>
        <td width="432" valign="top" style="background: #db1b34;">

            <table width="100%" cellspacing="0" cellpadding="0" border="0">

                <tr>
                    <td>
                        <p style="margin-bottom: 30px; margin-left: 30px;"><a href="http://budexperience.com"><img src="/mail/web_bud_alcfmail1/images/logo.jpg" width="213" height="79" alt="BUD" border="0" /></a></p>
                    </td>

                    <td align="right" valign="middle">
                        <p style="margin-bottom: 30px;">
                            <a href="{{ Config::get('social.fb') }}" style="margin-right: 20px;"><img src="/mail/web_bud_alcfmail1/images/ico-fb.jpg" width="20" height="20" border="0" alt="Facebook" /></a>
                            <a href="{{ Config::get('social.vk') }}" style="margin-right: 20px;"><img src="/mail/web_bud_alcfmail1/images/ico-vk.jpg" width="20" height="20" border="0" alt="ВКонтакте" /></a>
                            <a href="{{ Config::get('social.in') }}" style="margin-right: 20px;"><img src="/mail/web_bud_alcfmail1/images/ico-ins.jpg" width="20" height="20" border="0" alt="Instagram" /></a>
                        </p>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <p style="margin-top: 0; margin-bottom: 20px; margin-left: 130px; font-weight: bold; font-size: 31px; line-height: 100%; font-family: Arial; color: #ffffff;">
                            ПОЗДРАВЛЯЕМ!<br>
                            ТЫ МОЖЕШЬ ПОЛУЧИТЬ<br>
                            СВОЙ ПРИЗ В ЦЕНТРЕ<br>
                            ВЫДАЧИ ПРИЗОВ В СВОЕМ<br>
                            ГОРОДЕ В СЕНТЯБРЕ
                        </p>
                        <p style="margin-top: 0; margin-bottom: 30px; margin-left: 130px; font-size: 16px; line-height: 120%; font-family: Arial; color: #ffffff;">
                            НЕ ЗАБУДЬ ВЗЯТЬ<br>
                            С СОБОЙ СЕРТИФИКАТ И ПАСПОРТ.<br>
                            АДРЕСА ЦЕНТРОВ ВЫДАЧИ ПРИЗОВ<br>
                            СМОТРИ НА САЙТЕ
                            <a href="http://250bud.com" target="_blank" style="color: #292242;">250BUD.COM</a>
                        </p>
                    </td>
                </tr>
            </table>

        </td>

        <td width="206" valign="bottom" style="background: #d81f38;">
            <img src="/mail/web_bud_alcfmail1/images/btl-r.jpg" width="206" height="539" alt="" border="0" style="display: block;" />
        </td>
    </tr>

    <tr>
        <td width="432">
            <p style="margin-top: 10px;"><img src="/mail/web_bud_alcfmail1/images/label.jpg" width="102" height="102" alt="" border="0" align="right" style="display: block;" /></p>

            <p style="margin-top: 20px; margin-bottom: 15px; margin-left: 30px; font-size: 13px; line-height: 130%; font-family: 'pf_din_text_cond_prolight', Arial; color: #999999;">Хочешь всегда быть в курсе последних<br> новостей — добавь адрес stadium@email.bud.ru <br>в адресную книгу. Если хочешь отписаться <br> от рассылки, то пройди по <a href="/unsubscribe/{{ $hash }}" style="color: #999999;">ссылке</a></p>

            <p style="margin-top: 0; margin-bottom: 15px; margin-left: 30px; font-size: 13px; line-height: 130%; font-family: 'pf_din_text_cond_prolight', Arial; color: #999999;">*безалкогольный</p>
        </td>
        <td width="206" valign="top">
            <img src="/mail/web_bud_alcfmail1/images/btl-rb.jpg" width="206" height="126" alt="" border="0" style="display: block;" />
        </td>
    </tr>


</table>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-80826464-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>