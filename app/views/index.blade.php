<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>BUD Alcohol free</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" media="screen" href="style.min.css">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script>
<script src="js/bootstrap.modal.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/design.js"></script>
</head>

<body>

<div class="app">

<div class="container">

	<div class="top__logo">
		<a href="/"><img src="images/logo.png" alt="BUD Alcohol Free"></a>
	</div><!--.top__logo-->

	<div class="top__title">
		зарегистрируйся<br>
		и получи сертификат<span> **</span> <br>
		на спортивную <br>
		трансляцию на
		<a href="http://championat.com"><img src="images/logo-champ.png" alt="чемпионат www.championat.com"></a>
	</div>

	<div class="bud__form">
		<form action="" method="post">
			<div class="inp-group">
				<div class="error first-name">{{ $errors->first('first_name') }}</div>
				<input type="text" name="first_name" placeholder="Имя" class="inp-style" value="{{ Input::old('first_name') }}">
			</div><!--.inp-group-->

			<div class="inp-group">
                <div class="error birthday">{{ $errors->first('birthday') }}</div>
				<input type="text" name="birthday" placeholder="Дата рождения" class="inp-style" value="{{ Input::old('birthday') }}" id="birthday">
			</div><!--.inp-group-->

			<div class="inp-group">
                <div class="error phone">{{ $errors->first('phone') }}</div>
				<input type="text" name="phone" placeholder="Телефон" class="inp-style" value="{{ Input::old('phone') }}" id="phone">
			</div><!--.inp-group-->

			<div class="inp-group">
                <div class="error email">{{ $errors->first('email') }}</div>
				<input type="text" name="email" placeholder="Электронная почта" class="inp-style" value="{{ Input::old('email') }}">
			</div><!--.inp-group-->

			<div class="inp-group inp-agree">
                <div class="error accept">{{ $errors->first('accept') }}</div>
				<label>
					<input type="checkbox" name="accept" @if(Input::get('accept'))checked="checked" @endif>
					<span class="inp-check"></span>
                    <a href="files/Согласие_BUD.pdf" target="_blank"><span class="txt">Согласие на обработку<div class="hide_br"></div> персональных данных,<br> а также<div class="hide_br"></div> передачу третьим лицам</span></a>
				</label>
			</div>

			<input type="submit" value="Регистрация" data-analytics="registration">

			<div class="bud_rules">
				<a href="/files/rules.pdf">полные<br> правила<br> акции</a>
			</div><!--.bud_rules-->
		</form>

		<div class="cf"></div>

		<div class="label-alco">
			<img src="images/label-alco.png" alt="0% алк. Безалкогольная продукция">
		</div>

		<div class="bud_rules bud_rules__bt">
			<a href="/files/rules.pdf">полные<br> правила<br> акции</a>
		</div><!--.bud_rules-->

		<div class="cf"></div>

	</div><!--.bud__form-->


	<div class="txt_btl">© 2016 BUD</div>
	<div class="txt_btr">
		<p>*безалкогольный</p>
		<p>** сертификат получат<br> первые 500 участников</p>
	</div>

</div><!--.container-->

<div class="bud_social">
	<ul>
		<li class="fb"><a href="{{ Config::get('social.fb') }}">Facebook</a></li>
		<li class="vk"><a href="{{ Config::get('social.vk') }}">ВКонтакте</a></li>
		<li class="in"><a href="{{ Config::get('social.in') }}">Instagram</a></li>
	</ul>
</div><!--.bud_social-->

</div><!--.app-->



<div class="footer">
	<div class="footer__in">
		<div class="footer__bt">
			<div class="footer__bt-left">
				<p>&copy; 2017 Anheuser-Busch InBev. <a href="/terms.html" target="_blank">Положение о конфиденциальности, персональных данных и использовании файлов cookie</a> &nbsp; <a href="/about.html" target="_blank">О компании «Сан ИнБев»</a></p>
			</div><!--.footer__bt-left-->
			
			<div class="footer__bt-right">
				<p>Наслаждайтесь ответственно</p>
			</div><!--.footer__bt-right-->
		</div><!--.footer__bt-->
	</div><!--.footer__in-->
</div><!--.footer-->


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-80826464-1', 'auto');
    ga('send', 'pageview');

    $('[data-analytics="registration"]').click(function(){
        ga('send', 'event', 'buttons', 'click', 'Registration');
    });

</script>



<!-- Modal Age -->
<div class="modal modal_vam modal_info modal_age fade" id="modal_age" tabindex="-1" role="dialog" aria-labelledby="modal_age" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal_info__body">
			
			<h3>Вы должны быть старше 18, чтобы попасть на этот сайт</h3>

			<form id="check_18year">
				<div class="inp-group">
					<div class="inp-line">
						<input type="tel" name="age_day" value="" placeholder="ДД" maxlength="2" class="inp-style inp-day">
					</div><!--.inp-line-->

					<div class="inp-line">
						<input type="tel" name="age_month" value="" placeholder="ММ" maxlength="2" class="inp-style inp-month">
					</div><!--.inp-line-->

					<div class="inp-line">
						<input type="tel" name="age_year" value="" placeholder="ГГГГ" maxlength="4" class="inp-style inp-year">
					</div><!--.inp-line-->

					<div class="inp-line">
						<input type="submit" value="Подтвердить" class="btn-submit">
					</div><!--.inp-line-->
				</div><!--.inp-group-->
			</form>

			<h4>Наслаждайтесь ответственно.</h4>
			
			<div class="modal_age__copy">
				<p>&copy; 2017 Anheuser-Busch InBev. Заходя на сайт, вы соглашаетесь с <a href="/terms.html" target="_blank">положением о конфиденциальности, персональных данных и использовании файлов cookie</a></p>
			</div><!--.modal_age__copy-->

    	</div><!--.modal_info__body-->
    </div>
  </div>

  <div class="footer">
	<div class="footer__in">
		<div class="footer__bt">
			<div class="footer__bt-left">
				<p>&copy; 2017 Anheuser-Busch InBev. <a href="/terms.html" target="_blank">Положение о конфиденциальности, персональных данных и использовании файлов cookie</a> &nbsp; <a href="/about.html" target="_blank">О компании «Сан ИнБев»</a></p>
			</div><!--.footer__bt-left-->
		</div><!--.footer__bt-->
	</div><!--.footer__in-->
</div><!--.footer-->
</div>
<!-- end Modal Age -->


</body>
</html>