<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>BUD</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body bgcolor="#ffffff">

<table width="638" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">

    <tr>
        <td width="432" valign="top" style="background: #db1b34;">

            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td colspan="2">
                        <p style="margin-top: 12px; margin-bottom: 7px; margin-left: 130px; font-size: 13px; line-height: 130%; font-family: Arial; color: #292242;">Возникли проблемы при отоброжении письма?<br>
                            <a href="http://budexperience.com/done/{{ $ticket->hash }}" style="color: #292242;">Смотрите исходную версию на сайте</a></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p style="margin-bottom: 30px; margin-left: 30px;"><a href="http://budexperience.com"><img src="http://budexperience.com/mail/bud_alcfmail2/images/logo.jpg" width="213" height="79" alt="BUD" border="0" /></a></p>
                    </td>

                    <td align="right" valign="middle">
                        <p style="margin-bottom: 30px;">
                            <a href="{{ Config::get('social.fb') }}" style="margin-right: 20px;"><img src="http://budexperience.com/mail/bud_alcfmail2/images/ico-fb.jpg" width="20" height="20" border="0" alt="Facebook" /></a>
                            <a href="{{ Config::get('social.vk') }}" style="margin-right: 20px;"><img src="http://budexperience.com/mail/bud_alcfmail2/images/ico-vk.jpg" width="20" height="20" border="0" alt="ВКонтакте" /></a>
                            <a href="{{ Config::get('social.in') }}" style="margin-right: 20px;"><img src="http://budexperience.com/mail/bud_alcfmail2/images/ico-ins.jpg" width="20" height="20" border="0" alt="Instagram" /></a>
                        </p>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <p style="margin-top: 0; margin-bottom: 5px; margin-left: 130px; font-weight: bold; font-size: 18px; line-height: 100%; font-family: Arial; color: #ffffff;">ДОБРЫЙ ДЕНЬ, {{ $ticket->first_name }}!</p>
                        <p style="margin-top: 0; margin-bottom: 20px; margin-left: 130px; font-weight: bold; font-size: 32px; line-height: 100%; font-family: Arial; color: #ffffff;">ТВОЙ КОД {{ $code }}</p>

                        <p style="margin-top: 0; margin-bottom: 20px; margin-left: 130px; font-weight: bold; font-size: 16px; line-height: 120%; font-family: Arial; color: #ffffff;">Данный код дает возможность <br> бесплатно посмотреть одну <br> спортивную трансляцию на <a href="http://www.championat.com" style="color: #ffffff;">сайте</a></p>

                        <p style="margin-bottom: 60px; margin-left: 130px;">
                            <img src="http://budexperience.com/mail/bud_alcfmail2/images/ch.jpg" alt="Чемпионат" border="0" />
                        </p>

                        <p style="margin-top: 0; margin-bottom: 20px; margin-left: 130px; font-weight: bold; font-size: 18px; line-height: 130%; font-family: Arial; color: #ffffff;">ПРИЯТНОГО ПРОСМОТРА<br>
                            с BUD Alcohol Free!</p>

                    </td>
                </tr>
            </table>

        </td>

        <td width="206" valign="bottom" style="background: #d81f38;">
            <img src="http://budexperience.com/mail/bud_alcfmail2/images/btl-r.jpg" width="206" height="539" alt="" border="0" style="display: block;" />
        </td>
    </tr>

    <tr>
        <td width="432">
            <p style="margin-top: 10px;"><img src="http://budexperience.com/mail/bud_alcfmail2/images/label.jpg" width="102" height="102" alt="" border="0" align="right" style="display: block;" /></p>

            <p style="margin-top: 20px; margin-bottom: 15px; margin-left: 30px; font-size: 13px; line-height: 130%; font-family: Arial; color: #999999;">Хочешь всегда быть в курсе последних<br> новостей — добавь адрес stadium@email.bud.ru <br>в адресную книгу. Если хочешь отписаться <br> от рассылки, то пройди по <a href="http://budexperience.com/unsubscribe/{{ $ticket->hash }}" style="color: #999999;">ссылке</a></p>

            <p style="margin-top: 0; margin-bottom: 15px; margin-left: 30px; font-size: 13px; line-height: 130%; font-family: Arial; color: #999999;">*безалкогольный</p>
        </td>
        <td width="206" valign="top">
            <img src="http://budexperience.com/mail/bud_alcfmail2/images/btl-rb.jpg" width="206" height="126" alt="" border="0" style="display: block;" />
        </td>
    </tr>


</table>

</body>
</html>