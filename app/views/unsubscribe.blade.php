<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>BUD Alcohol free</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" media="screen" href="/style.css">
    <!--[if IE]><script src="/js/html5.js"></script><![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="/js/design.js"></script>
</head>

<body>

<div class="container">

    <div class="top__logo">
        <a href="/"><img src="/images/logo.png" alt="BUD Alcohol Free"></a>
    </div><!--.top__logo-->


    <div class="bud_msgs">
        <div class="bud_msgs__in">
            <h2>Скоро ты будешь<br> отписан от нашей<br> рассылки</h2>
        </div><!--.bud_msgs__in-->
    </div><!--.bud_msgs-->


    <div class="txt_btl">© 2016 BUD</div>
    <div class="txt_btr">*безалкогольный</div>

</div><!--.container-->

<div class="bud_social">
    <ul>
        <li class="fb"><a href="{{ Config::get('social.fb') }}">Facebook</a></li>
        <li class="vk"><a href="{{ Config::get('social.vk') }}">ВКонтакте</a></li>
        <li class="in"><a href="{{ Config::get('social.in') }}">Instagram</a></li>
    </ul>
</div><!--.bud_social-->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-80826464-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>