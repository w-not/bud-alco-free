<?php

class AppController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function IndexAction()
	{
		if(Code::where('used', '=', 0)->count() < 1) {
			return View::make('nocodes');
		}
		
		return View::make('index');
	}

	public function CreateTicketAction()
	{
		$input = Input::all();
		$validator = Validator::make($input, [
				'first_name' => ['required', 'string'],
				'email' => ['required', 'string', 'email', 'unique:tickets'],
				'phone' => ['required', 'regex:/^\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/i'],
				'birthday' => ['required', 'date_format:' . Ticket::DATE_FORMAT, 'after:1900-01-01'],
				'accept' => 'accepted',
		]);

		if ($validator->fails()) {
			return Redirect::to('/')
				->withErrors($validator)
				->withInput();
		}

		$email = $input['email'];
		$input['birthday'] = date_format(date_create_from_format(Ticket::DATE_FORMAT, $input['birthday']), 'Y-m-d');
		$input['hash'] = hash_hmac('sha256', $email, Config::get('app.key'));
		$ticket = Ticket::create($input);

		Mail::send('emails.confirm', ['hash' => $ticket->hash], function($message) use ($email) {
			$message->to($email)->subject('BUD Alcohol Free');
		});

		return Redirect::action('AppController@ConfirmedAction');
	}

	public function ConfirmedAction()
	{
		return View::make('confirmed');
	}

	public function CodeAction($code)
	{
		$ticket = Ticket::where('hash', '=', $code)
			->where('code_sent', '=', 0)
			->first();

		if($ticket) {
			$email = $ticket->email;
			$code = Code::where('used', '=', 0)->first();
			if($code) {
				Mail::send('emails.code', ['code' => $code->code, 'ticket' => $ticket], function($message) use ($email) {
					$message->to($email)->subject('BUD Alcohol Free');
				});

				$ticket->code_sent = 1;
				$ticket->save();

				$code->used = 1;
				$code->ticket_id = $ticket->id;
				$code->save();

				if(Code::where('used', '=', 1)->count() == 400) {
					$notice_to = ['anna.s@w-not.ru', 'alice@w-not.ru', 'ivan.z@w-not.ru'];
					foreach($notice_to as $email_notice) {
						Mail::send('emails.notice', [], function ($message) use ($email_notice) {
							$message->to($email_notice)->subject('BUD Alcohol Free: зарегистрировано 400 кодов!');
						});
					}
				}
			}
		}

		return View::make('code_sent');
	}

    public function ConfirmAction($hash)
    {
        return View::make('confirm', compact('hash'));
    }

    public function getPrizeAction($hash)
    {
        return View::make('get_prize', compact('hash'));
    }

    public function DoneAction($hash)
    {
        if($ticket = Ticket::where('hash', '=', $hash)->first()) {
            return View::make('done', compact('ticket'));
        }
        return Redirect::action('AppController@IndexAction');
    }

    public function UnsubscribeAction($hash)
    {
        if($ticket = Ticket::where('hash', '=', $hash)->first()) {
            if(!Unsubscribe::where('ticket_id', '=', $ticket->id)->count()) {
                Unsubscribe::create(['ticket_id' => $ticket->id]);
            }
        }
        return View::make('unsubscribe');
    }
}
