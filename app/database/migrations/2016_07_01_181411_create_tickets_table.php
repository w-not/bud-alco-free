<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function (Blueprint $table) {
			$table->increments('id');
			$table->string('hash');
			$table->string('first_name');
			$table->string('middle_name');
			$table->string('last_name');
			$table->string('email')->unique();
			$table->date('birthday');
			$table->string('sex');
			$table->string('phone');
			$table->boolean('code_sent');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
