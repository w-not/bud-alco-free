<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'AppController@IndexAction');

Route::post('/', 'AppController@CreateTicketAction');

Route::get('/code/{hash}', 'AppController@CodeAction');

Route::get('/confirmed', 'AppController@ConfirmedAction');

Route::get('/confirm/{hash}', 'AppController@ConfirmAction');

Route::get('/get-prize/{hash}', 'AppController@getPrizeAction');

Route::get('/done/{hash}', 'AppController@DoneAction');

Route::get('/unsubscribe/{hash}', 'AppController@UnsubscribeAction');