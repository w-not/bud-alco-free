<?php

class Ticket extends  Eloquent
{
    const DATE_FORMAT = 'd.m.Y';

    protected $table = 'tickets';

    protected $fillable = array('first_name', 'birthday', 'email', 'phone', 'hash');

    public $timestamps = false;

    public function code()
    {
        return $this->hasOne('Code');
    }

}