<?php

class Code extends  Eloquent
{
    protected $table = 'codes';

    public $timestamps = false;
}