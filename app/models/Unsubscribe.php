<?php

class Unsubscribe extends  Eloquent
{
    protected $table = 'unsubscribe';

    protected $fillable = array('ticket_id');

    public function ticket()
    {
        return $this->belongsTo('App\Ticket', 'id_ticket');
    }
}